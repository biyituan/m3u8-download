## M3U8多线程下载器
- 多线程
- Proxy
- 支持Windows、MacOS

## 预览
![1](https://p.pstatp.com/origin/fecc00037fd540943e11)


![2](https://p.pstatp.com/origin/ff690002983e80553266)


![3](https://p.pstatp.com/origin/fe4b0002d88ff40fabc1)


## 配置

- 基本
  - 同时下载 `多线程同时下载多个分片（建议根据网速配置，并不是越大越好）`
  - 自动合并 `下载完自动合并分片`
  - 删除分片 `合并完成后自动删除分片`
  - 重试次数 `下载失败重试的次数`
- Proxy
  - HTTP
  - SOCKS
- 请求头
  - 自定义请求头


## 下载地址(Windows & MacOS)
- [1.0.2 点我下载](https://lanzoui.com/b04sp695i)
    - Proxy
    - 本地M3U8下载
    - 解密支持向量
    - 自定义并发数
    - 重试次数
    - 分片下载百分比
- [1.0.1](https://pan.lanzous.com/iInXIfqaded)
    - 删除部分无用链接
- ~~1.0.0~~

## 自定义 JRE
1. [https://openjfx.io/](https://openjfx.io/) 下载对应版本 [jmods](https://gluonhq.com/download/javafx-14-jmods-mac/) ，解压到 jdk11+(jmods文件夹内)
2. 执行shell：`jlink --strip-debug --compress 2 --no-header-files --no-man-pages --add-modules java.base,javafx.base,javafx.controls,javafx.fxml,java.naming,java.sql  --output jre11`

## 常见问题
1. 为什么有些 M3U8 无法下载？
    1. M3U8文件、Key、ts文件经过特殊加密
    2. 没有权限访问URL尝试配置请求头
2. MacOS 为什么无法打开
    1. 给权限：`sudo chmod -R 777 ./`
    2. 终端启动：`./start-mac.sh` 或 `bash ./start-mac.sh` 
