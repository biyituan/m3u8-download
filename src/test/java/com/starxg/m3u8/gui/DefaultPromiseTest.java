package com.starxg.m3u8.gui;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DefaultPromiseTest {
    /**
     * 
     */
    @Test
    public void test() throws InterruptedException {
        new Promise<>((resolve, reject) -> {
            reject.accept(new RuntimeException());
            resolve.accept("succeeded");
            resolve.accept("succeeded");
        }).then(e -> {
            log.info("then:{}", e);
        }).error(e -> {
            log.error("error:{}", e);
        }).complete(() -> {
            log.info("complete");
        });
        Thread.currentThread().join();
    }
}