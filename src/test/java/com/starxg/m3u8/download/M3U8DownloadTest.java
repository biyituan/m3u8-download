package com.starxg.m3u8.download;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;

import org.junit.Test;

import com.starxg.m3u8.utils.EncryptUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class M3U8DownloadTest {

    @Test
    public void test() throws Exception {
        FileChannel channel = new FileInputStream("C:\\Users\\star\\Desktop\\新建文件夹\\1.ts").getChannel();
        ByteBuffer buffer = EncryptUtils.decrypt(test2(), channel, EncryptUtils.iv("0xbddc5acb84f5b7fd2e08670b84245ab1"));
        new FileOutputStream("C:\\Users\\star\\Desktop\\新建文件夹\\1out.ts").getChannel().write(buffer);
    }

    public byte[] test2() {
        int[] keys = new int[] { 118,45,114,175,215,68,51,63,42,148,73,200,104,178,214,190 };
        byte[] key = new byte[16];
        for (int i = 0; i < keys.length; i++) {
            key[i] = (byte) keys[i];
        }
        System.out.println(Arrays.toString(key));
        return key;
    }
}