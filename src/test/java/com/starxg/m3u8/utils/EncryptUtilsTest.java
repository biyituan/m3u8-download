package com.starxg.m3u8.utils;

import junit.framework.TestCase;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.Base64;

public class EncryptUtilsTest{
    @Test
public void test() throws IOException {
        byte[] iv = Base64.getDecoder().decode("iU+nDC+GE1d7va0p0yMwxQ==");
        byte[] key = Base64.getDecoder().decode("RlBkejNSdFFRekNxbEN0Vw==");

        byte[] iv2 = EncryptUtils.iv("0xe01bf023061608d829c19d9488e5c5f7");
        System.out.println(Arrays.toString(iv));
        System.out.println(Arrays.toString(iv2));
        System.out.println(Arrays.toString(Base64.getDecoder().decode("I/Ab4NgIFgaUncEp98XliA==")));
        FileChannel channel = new FileInputStream("C:\\Users\\Administrator\\Downloads\\000000.ts").getChannel();
        ByteBuffer decrypt = EncryptUtils.decrypt(key, channel, iv);
        new RandomAccessFile("C:\\Users\\Administrator\\Downloads\\000000.out.ts","rw").getChannel().write(decrypt);
    }
}