package com.starxg.m3u8.utils;

import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class ThreadPoolUtilsTest {
    /**
     * 
     */
    @Test
    public void test() throws ExecutionException, InterruptedException {
        ThreadPoolUtils.submit(() -> {
            System.out.println(1);
        }, 0, 1, TimeUnit.SECONDS).get();
    }
}