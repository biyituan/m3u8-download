package com.starxg.m3u8.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import com.starxg.m3u8.parser.M3U8Item;
import com.starxg.m3u8.parser.M3U8Parser;
import com.starxg.m3u8.parser.M3U8Text;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;
import okhttp3.*;

@Slf4j
public class OkHttpUtilsTest {

    @Test
    public void test() throws Exception {
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES).readTimeout(1, TimeUnit.MINUTES).followRedirects(true)
                .followSslRedirects(true).build();
        M3U8Text m3u8 = (M3U8Text) M3U8Parser
                .parse("https://cdn.jsdelivr.net/gh/YQHP-Happi/soluow-resouces-repos@cinema/m3u8/2020/7/釜山行2-1.m3u8");
        List<Call> calls = new ArrayList<>();
        for (M3U8Item item : m3u8.getItems()) {
            Request.Builder builder = new Request.Builder();
            builder.url(item.getUrl());
            Call call = client.newCall(builder.build());
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    try (InputStream is = Objects.requireNonNull(response.body()).byteStream()) {
                        int len;
                        byte[] bytes = new byte[1024 * 8];
                        int read = 0;
                        while ((len = is.read(bytes)) > -1) {

                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
            calls.add(call);
        }

        ThreadPoolUtils.submit(() -> {
            for (Call call : calls) {
                call.cancel();
            }
        }, 3, TimeUnit.SECONDS);

        Thread.currentThread().join();
    }

    @Test
    public void testSpeed() throws InterruptedException {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequestsPerHost(300);
        dispatcher.setMaxRequests(Integer.MAX_VALUE);
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES).readTimeout(1, TimeUnit.MINUTES).followRedirects(true)
                .dispatcher(dispatcher)
                .followSslRedirects(true).build();

        int total = 30000;
        CountDownLatch latch = new CountDownLatch(total);
        StopWatch watch = new StopWatch();
        watch.start();
        for (int i = 0; i < total; i++) {
            Request request = HttpUtils.prepareGet(
                    "http://api.weixin.qq.com/cgi-bin/media/voice/addvoicetorecofortext")
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    latch.countDown();
                }

                @Override
                public void onResponse(Call call, Response response) {
                    response.close();
                    latch.countDown();
                }
            });
        }
        latch.await();
        watch.stop();
        log.info("watch:{}", watch.getTime());
    }
}
