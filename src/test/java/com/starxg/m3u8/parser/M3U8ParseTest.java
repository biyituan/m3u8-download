package com.starxg.m3u8.parser;

import java.io.InputStream;
import java.net.URL;

import okhttp3.Response;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.alibaba.fastjson.JSON;

public class M3U8ParseTest {

    @Test
    public void aliyunTest() throws Exception {
        InputStream is = getClass().getResourceAsStream("/aliyun.m3u8");
        String html = IOUtils.toString(is, "utf-8");
        M3U8Text m3u8 = (M3U8Text) M3U8Parser.parse(new URL("https://edu.aliyun.com/"), html);
        M3U8Item item = m3u8.getItems().get(0);
        System.out.println(item.getKey());
        test(item.getUrl().toString(), item.getKey());
    }

    @Test
    public void noKeyTest() throws Exception {
        InputStream is = getClass().getResourceAsStream("/nokey.m3u8");
        String html = IOUtils.toString(is, "utf-8");
        M3U8Text m3u8 = (M3U8Text) M3U8Parser.parse(new URL("https://edu.aliyun.com/"), html);
        test(m3u8.getItems().get(0).getUrl().toString(), m3u8.getKey());
    }

    @Test
    public void hasKeyTest() throws Exception {
        InputStream is = getClass().getResourceAsStream("/haskey.m3u8");
        String html = IOUtils.toString(is, "utf-8");
        M3U8 m3u8 = M3U8Parser.parse(new URL("https://a.mahua-kb.com/ppvod/922PZsE3/"), html);
        System.out.println(JSON.toJSONString(m3u8));
    }

    @Test
    public void liveTest() throws Exception {
        M3U8 m3u8 = M3U8Parser.parse("http://ivi.bupt.edu.cn/hls/cctv1hd.m3u8");
        System.out.println(JSON.toJSONString(m3u8));
    }

    private void test(String url, M3U8Key key) throws Exception {
    }
}
