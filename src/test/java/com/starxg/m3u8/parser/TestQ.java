package com.starxg.m3u8.parser;

import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

import com.starxg.m3u8.utils.ThreadPoolUtils;
import org.junit.Test;

public class TestQ {
    /**
     * 
     */
    @Test
    public void test() throws InterruptedException {

        LinkedBlockingQueue<M3U8Item> queue = new LinkedBlockingQueue<>();
        Future<?> submit = ThreadPoolUtils.submit(() -> {
            while (true) {
                try {
                    queue.take();
                } catch (InterruptedException e) {
                    return 1;
                }
            }
        });
        ThreadPoolUtils.execute(() -> {
            try {
                Thread.sleep(2000L);
                System.out.println(submit.cancel(true));
                System.out.println(submit.isCancelled());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread.currentThread().join();
    }
}
