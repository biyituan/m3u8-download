package com.starxg.m3u8.parser;

import java.util.concurrent.TimeUnit;

import com.starxg.m3u8.utils.ThreadPoolUtils;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class M3U8LiveTest {
    @Test
    public void liveTest() throws Exception {
        M3U8Live m3u8 = (M3U8Live) M3U8Parser.parse("http://ivi.bupt.edu.cn/hls/cctv1hd.m3u8");
        System.out.println(m3u8.getKey());
        System.out.println(m3u8.getItems());

        ThreadPoolUtils.submit(() -> {
            try {
                log.info("开始刷新");
                M3U8Live m = (M3U8Live) M3U8Parser.parse(m3u8.getUrl());
                m3u8.getItems().addAll(m.getItems());
                Thread.sleep(4000L);
                log.info("数量：{}", m3u8.getItems().size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 0, 3, TimeUnit.SECONDS);

        Thread.currentThread().join();
    }
}