package com.starxg.m3u8.exception;
/**
 * M3U8Exception
 * @author huangxingguang@lvmama.com
 * @date 2020-08-11 11:27
 */
public class M3U8Exception extends RuntimeException{
    public M3U8Exception() {
    }

    public M3U8Exception(String message) {
        super(message);
    }

    public M3U8Exception(String message, Throwable cause) {
        super(message, cause);
    }

    public M3U8Exception(Throwable cause) {
        super(cause);
    }

    public M3U8Exception(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
