package com.starxg.m3u8.exception;

/**
 * 读取本地文件夹时里面没有m3u8的下载地址
 * 
 * @author huangxingguang@lvmama.com
 * @date 2020-08-25 11:22
 */
public class FileDownloadUrlException extends M3U8Exception {
    public FileDownloadUrlException() {
    }

    public FileDownloadUrlException(String message) {
        super(message);
    }

    public FileDownloadUrlException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileDownloadUrlException(Throwable cause) {
        super(cause);
    }

    public FileDownloadUrlException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
