package com.starxg.m3u8.gui;

import java.util.function.Consumer;

@FunctionalInterface
public interface PromiseCallback<T> {

    void accept(Consumer<T> resolve, Consumer<Throwable> reject) throws Exception;
}
