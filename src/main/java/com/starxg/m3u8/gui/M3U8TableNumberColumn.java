package com.starxg.m3u8.gui;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;

/**
 * M3U8TableNumberColumn
 *
 * @author huangxingguang@lvmama.com
 * @date 2020-08-19 18:24
 */
public class M3U8TableNumberColumn<S, T> extends M3U8TableColumn<S, T>
        implements Callback<TableColumn<S, T>, TableCell<S, T>> {

    public M3U8TableNumberColumn(String text) {
        super(text);
        setCellFactory(this);
    }

    public static class NumberTableCell<S, T> extends TableCell<S, T> {

        private final int startNumber;

        NumberTableCell(int startNumber) {
            this.startNumber = startNumber;
        }

        @Override
        public void updateItem(T item, boolean empty) {
            super.updateItem(item, empty);
            setText(empty ? StringUtils.EMPTY : Integer.toString(startNumber + getIndex()));
        }

    }

    @Override
    public TableCell<S, T> call(TableColumn<S, T> param) {
        return new NumberTableCell<>(1);
    }

}