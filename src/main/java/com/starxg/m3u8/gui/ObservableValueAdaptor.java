package com.starxg.m3u8.gui;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * ObservableValueAdaptor
 * @author huangxingguang@lvmama.com
 * @date 2020-08-13 22:50
 */
public class ObservableValueAdaptor<T> implements ObservableValue<T> {
    private ObservableValue<T> observableValue;

    public ObservableValueAdaptor(ObservableValue<T> observableValue) {
        this.observableValue = observableValue;
    }

    @Override
    public void addListener(ChangeListener<? super T> listener) {
        observableValue.addListener(listener);
    }

    @Override
    public void removeListener(ChangeListener<? super T> listener) {
        observableValue.removeListener(listener);
    }

    @Override
    public T getValue() {
        return observableValue.getValue();
    }

    @Override
    public void addListener(InvalidationListener listener) {
        observableValue.addListener(listener);
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        observableValue.removeListener(listener);
    }
}
