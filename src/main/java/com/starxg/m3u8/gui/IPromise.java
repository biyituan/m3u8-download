package com.starxg.m3u8.gui;

import java.util.function.Consumer;

/**
 * IPromise
 * 
 * @author huangxingguang@lvmama.com
 * @date 2020-08-13 17:47
 */
public interface IPromise<T> {
    IPromise<T> then(Consumer<T> t);

    IPromise<T> error(Consumer<Throwable> t);

    IPromise<T> complete(Runnable t);

    IPromise<T> complete();

    void cancel();
}
