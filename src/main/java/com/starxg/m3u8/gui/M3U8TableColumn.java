package com.starxg.m3u8.gui;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * M3U8TableColumn
 * @author huangxingguang@lvmama.com
 * @date 2020-08-13 00:12
 */
public class M3U8TableColumn<S, T> extends TableColumn<S, T> {
    public M3U8TableColumn(String text) {
        super(text);
        setReorderable(false);
        setResizable(false);
        setSortable(false);
    }


}
