package com.starxg.m3u8.gui;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import javafx.application.Platform;
import lombok.extern.slf4j.Slf4j;

/**
 * M3U8Platform
 * 
 * @author huangxingguang@lvmama.com
 * @date 2020-08-13 18:24
 */
@Slf4j
public class M3U8Platform {
    public static <T> T runLaterAndReturn(Callable<T> callable) {

        CountDownLatch latch = new CountDownLatch(1);
        var ref = new Object() {
            T data = null;
        };

        Platform.runLater(() -> {
            try {
                ref.data = callable.call();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            } finally {
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException ignored) {

        }
        return ref.data;
    }
}
