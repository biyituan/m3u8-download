package com.starxg.m3u8.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.starxg.m3u8.M3U8Application;
import com.starxg.m3u8.config.ApplicationConfig;
import com.starxg.m3u8.controller.BaseController;
import com.starxg.m3u8.exception.M3U8Exception;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * DialogUtils
 *
 * @author huangxingguang@lvmama.com
 * @date 2020-08-13 23:27
 */
public class DialogUtils {

    public static void alert(String message, Window stage) {
        alert(message, Alert.AlertType.INFORMATION, stage);
    }

    public static boolean confirm(String message, Window stage) {
        return alert(message, Alert.AlertType.CONFIRMATION, stage).orElse(ButtonType.NO)
                .getButtonData() != ButtonBar.ButtonData.CANCEL_CLOSE;
    }

    public static String prompt(String message, String defaultValue, Window stage) {
        TextInputDialog dialog = new TextInputDialog(defaultValue);
        dialog.getEditor().setPromptText(message);
        dialog.setTitle("提示");
        dialog.getDialogPane().setMinHeight(70);
        dialog.getDialogPane().setMinWidth(450);
        dialog.initOwner(stage);
        dialog.setHeaderText(null);
        dialog.setContentText(message);
        return dialog.showAndWait().orElse(null);
    }

    public static Optional<ButtonType> alert(String message, Alert.AlertType type, Window stage) {
        Alert alert = new Alert(type);
        alert.setContentText(message);
        alert.setHeaderText(null);
        alert.initOwner(stage);
        alert.setTitle("提示");
        return alert.showAndWait();
    }

    public static File showOpenFileDialog(String title, File init, Window stage, FileChooser.ExtensionFilter filter) {
        FileChooser chooser = fileChooserDialog(title, init, stage, filter);
        File file = chooser.showOpenDialog(stage);
        if (file != null) {
            ApplicationConfig.getInstance().getApp().setLastOpenPath(FilenameUtils.getPath(file.getAbsolutePath()));
        }
        return file;
    }

    public static File showSaveFileDialog(String title, File init, Window stage, FileChooser.ExtensionFilter filter) {
        FileChooser chooser = fileChooserDialog(title, init, stage, filter);
        File file = chooser.showSaveDialog(stage);
        if (file != null) {
            ApplicationConfig.getInstance().getApp().setLastOpenPath(FilenameUtils.getPath(file.getAbsolutePath()));
        }
        return file;
    }

    private static FileChooser fileChooserDialog(String title, File init, Window stage, FileChooser.ExtensionFilter filter) {
        FileChooser chooser = new FileChooser();
        chooser.setTitle(StringUtils.defaultString(title, "请选择 M3U8 文件"));
        chooser.setInitialDirectory(ObjectUtils.defaultIfNull(init, initDialogPath()));
        if (filter != null) {
            chooser.getExtensionFilters().add(filter);
        }
        return chooser;
    }

    public static Dialog<Void> openDialog(String fxml, Window stage) {
        Dialog<Void> dialog = new Dialog<>();
        try {
            FXMLLoader loader = new FXMLLoader(M3U8Application.class.getResource(fxml));
            Parent root = loader.load();
            Object c = loader.getController();
            if (c instanceof BaseController) {
                ((BaseController) c).setStage(stage);

            }
            dialog.getDialogPane().setContent(root);
            dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);
            dialog.getDialogPane().setContent(root);
            dialog.setResizable(true);
            dialog.initOwner(stage);
            return dialog;
        } catch (IOException ex) {
            throw new M3U8Exception(ex.getMessage());
        }
    }

    public static File showDialog(String title, File init, Window stage) {
        ApplicationConfig config = ApplicationConfig.getInstance();
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle(StringUtils.defaultString(title, "请选择保存的文件夹"));
        chooser.setInitialDirectory(ObjectUtils.defaultIfNull(init, initDialogPath()));
        File file = chooser.showDialog(stage);
        if (file != null) {
            config.getApp().setLastOpenPath(file.getAbsolutePath());
        }
        return file;
    }

    private static File initDialogPath() {
        File initDir = new File(ApplicationConfig.getInstance().getApp().getLastOpenPath());
        if (!initDir.exists()) {
            initDir = new File(System.getProperty("user.home"));
        }
        return initDir;
    }
}
