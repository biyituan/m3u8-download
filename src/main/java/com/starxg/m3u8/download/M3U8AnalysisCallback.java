package com.starxg.m3u8.download;

import com.starxg.m3u8.parser.M3U8;

public interface M3U8AnalysisCallback {
    /**
     * 开始执行之前
     */
    default void before() {
    }

    /**
     * 失败
     */
    default void failed(Throwable e) {
    }


    /**
     * 失败
     */
    default void succeeded(M3U8 m3u8) {
    }


    /**
     * 结束
     */
    default void completed() {
    }


}
