package com.starxg.m3u8.download;

import java.io.File;
import java.net.URL;

import com.starxg.m3u8.parser.M3U8;
import com.starxg.m3u8.parser.M3U8Parser;
import com.starxg.m3u8.utils.ThreadPoolUtils;

import javafx.application.Platform;

public class M3U8Analysis {
    private URL url;
    private File file;

    public M3U8Analysis(URL url) {
        this.url = url;
    }

    public M3U8Analysis(URL url, File file) {
        this(url);
        this.file = file;
    }

    public void start(M3U8AnalysisCallback callback) {
        callback.before();
        ThreadPoolUtils.execute(() -> {
            try {

                M3U8 m3u8;
                // 判断是否是文件
                if (file != null) {
                    m3u8 = M3U8Parser.parse(url, file);
                } else {
                    m3u8 = M3U8Parser.parse(url);
                }

                Platform.runLater(() -> callback.succeeded(m3u8));
            } catch (Exception e) {
                Platform.runLater(() -> callback.failed(e));
            } finally {
                Platform.runLater(callback::completed);
            }
        });
    }

}
