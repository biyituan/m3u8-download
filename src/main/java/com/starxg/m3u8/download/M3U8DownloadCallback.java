package com.starxg.m3u8.download;

import com.starxg.m3u8.parser.M3U8Item;

/**
 * M3U8DownloadCallback
 *
 * @author huangxingguang@lvmama.com
 * @date 2020-08-20 16:07
 */
public interface M3U8DownloadCallback<T> {

    /**
     * 开始执行之前
     */
    default void before(T before) {
    }

    /**
     * 下载完成
     *
     */
    default void succeeded() {
    }

    /**
     * 分片下载完成
     *
     * @param item
     *            M3U8Item
     */
    default void part(M3U8Item item) {
    }

    /**
     * 分片下载失败
     *
     * @param item
     *            M3U8Item
     */
    default void part(M3U8Item item, Throwable e) {
    }

    /**
     * 失败
     */
    default void failed(Throwable e) {
    }

    /**
     * 下载停止
     */
    default void stopped() {
    }

    /**
     * 下载结束
     */
    default void completed() {
    }

    /**
     * 下载进度
     *
     * @param item
     *            item
     * @param total
     *            total
     * @param read
     *            read
     * @param process
     *            process
     */
    default void process(M3U8Item item, long total, long read, double process) {
    }

}
