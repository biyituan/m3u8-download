package com.starxg.m3u8.config;

import org.apache.commons.lang3.StringUtils;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * AppConfig
 * 
 * @author huangxingguang@lvmama.com
 * @date 2020-08-19 13:01
 */
public class AppConfig {

    public static final int HTTP_MAX_CONNECTIONS = 50;

    /**
     * 最后打开的路径
     */
    private SimpleStringProperty lastOpenPath = new SimpleStringProperty(StringUtils.EMPTY);

    /**
     * 请求头
     */
    private SimpleStringProperty requestHeader = new SimpleStringProperty(
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0");

    /**
     * 下载数
     */
    private SimpleIntegerProperty httpConnections = new SimpleIntegerProperty(5);

    /**
     * 重试次数
     */
    private SimpleIntegerProperty httpRetryCount = new SimpleIntegerProperty(3);

    /**
     * 自动合并分片
     */
    private SimpleBooleanProperty autoMergeTs = new SimpleBooleanProperty(true);

    /**
     * 自动删除分片
     */
    private SimpleBooleanProperty autoDeleteTs = new SimpleBooleanProperty(true);

    AppConfig() {
    }

    public String getLastOpenPath() {
        return lastOpenPath.get();
    }

    public SimpleStringProperty lastOpenPathProperty() {
        return lastOpenPath;
    }

    public void setLastOpenPath(String lastOpenPath) {
        this.lastOpenPath.set(lastOpenPath);
    }

    public String getRequestHeader() {
        return requestHeader.get();
    }

    public SimpleStringProperty requestHeaderProperty() {
        return requestHeader;
    }

    public void setRequestHeader(String requestHeader) {
        this.requestHeader.set(requestHeader);
    }

    public int getHttpConnections() {
        return httpConnections.get();
    }

    public SimpleIntegerProperty httpConnectionsProperty() {
        return httpConnections;
    }

    public void setHttpConnections(int httpConnections) {
        this.httpConnections.set(httpConnections);
    }

    public boolean isAutoMergeTs() {
        return autoMergeTs.get();
    }

    public SimpleBooleanProperty autoMergeTsProperty() {
        return autoMergeTs;
    }

    public void setAutoMergeTs(boolean autoMergeTs) {
        this.autoMergeTs.set(autoMergeTs);
    }

    public boolean isAutoDeleteTs() {
        return autoDeleteTs.get();
    }

    public SimpleBooleanProperty autoDeleteTsProperty() {
        return autoDeleteTs;
    }

    public void setAutoDeleteTs(boolean autoDeleteTs) {
        this.autoDeleteTs.set(autoDeleteTs);
    }

    public int getHttpRetryCount() {
        return httpRetryCount.get();
    }

    public SimpleIntegerProperty httpRetryCountProperty() {
        return httpRetryCount;
    }

    public void setHttpRetryCount(int httpRetryCount) {
        this.httpRetryCount.set(httpRetryCount);
    }
}