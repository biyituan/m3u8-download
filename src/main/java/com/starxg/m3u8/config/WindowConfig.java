package com.starxg.m3u8.config;

import org.apache.commons.lang3.StringUtils;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

import java.net.Proxy;

/**
 * WindowConfig
 * 
 * @author huangxingguang@lvmama.com
 * @date 2020-08-19 13:01
 */
public class WindowConfig {
    public static final String PROXY_NONE = "无";

    /**
     * 高
     */
    private SimpleDoubleProperty height = new SimpleDoubleProperty(410);

    /**
     * 宽
     */
    private SimpleDoubleProperty weight = new SimpleDoubleProperty(600);

    /**
     * 标题
     */
    private SimpleStringProperty title = new SimpleStringProperty("M3U8下载器");

    /**
     * 代理host
     */
    private SimpleStringProperty proxyHost = new SimpleStringProperty("127.0.0.1");

    /**
     * 代理类型
     */
    private SimpleStringProperty proxyType = new SimpleStringProperty(WindowConfig.PROXY_NONE);

    /**
     * 代理端口
     */
    private SimpleStringProperty proxyPort = new SimpleStringProperty("80");

    /**
     * 代理用户名
     */
    private SimpleStringProperty proxyUsername = new SimpleStringProperty(StringUtils.EMPTY);

    /**
     * 代理密码
     */
    private SimpleStringProperty proxyPassword = new SimpleStringProperty(StringUtils.EMPTY);

    WindowConfig() {
    }

    public double getHeight() {
        return height.get();
    }

    public SimpleDoubleProperty heightProperty() {
        return height;
    }

    public void setHeight(double height) {
        this.height.set(height);
    }

    public double getWeight() {
        return weight.get();
    }

    public SimpleDoubleProperty weightProperty() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight.set(weight);
    }

    public String getTitle() {
        return title.get();
    }

    public SimpleStringProperty titleProperty() {
        return title;
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public String getProxyHost() {
        return proxyHost.get();
    }

    public SimpleStringProperty proxyHostProperty() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost.set(proxyHost);
    }

    public String getProxyUsername() {
        return proxyUsername.get();
    }

    public SimpleStringProperty proxyUsernameProperty() {
        return proxyUsername;
    }

    public void setProxyUsername(String proxyUsername) {
        this.proxyUsername.set(proxyUsername);
    }

    public String getProxyPassword() {
        return proxyPassword.get();
    }

    public SimpleStringProperty proxyPasswordProperty() {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword.set(proxyPassword);
    }

    public String getProxyPort() {
        return proxyPort.get();
    }

    public SimpleStringProperty proxyPortProperty() {
        return proxyPort;
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort.set(proxyPort);
    }

    public String getProxyType() {
        return proxyType.get();
    }

    public SimpleStringProperty proxyTypeProperty() {
        return proxyType;
    }

    public void setProxyType(String proxyType) {
        try {
            Proxy.Type.valueOf(proxyType);
        } catch (IllegalArgumentException e) {
            proxyType = PROXY_NONE;
        }
        this.proxyType.set(proxyType);
    }
}