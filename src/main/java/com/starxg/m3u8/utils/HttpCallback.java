package com.starxg.m3u8.utils;

import okhttp3.Call;
import okhttp3.Response;

/**
 * HttpCallback
 * 
 * @author huangxingguang@lvmama.com
 * @date 2020-08-21 14:10
 */
public interface HttpCallback {
    void onFailure(Call call, Throwable e);

    void onResponse(Call call, Response response) throws Exception;

    void completed();
}
