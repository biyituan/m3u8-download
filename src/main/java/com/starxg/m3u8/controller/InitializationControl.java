package com.starxg.m3u8.controller;

public interface InitializationControl<T extends BaseController> {
    /**
     * 初始化元素并传入controller
     *
     * @param controller controller
     */
    void init(T controller);
}
