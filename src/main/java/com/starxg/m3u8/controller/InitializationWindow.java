package com.starxg.m3u8.controller;

public interface InitializationWindow {
    default void initView() {

    }

    default void initData() {

    }

    default void initEvent() {

    }
}
