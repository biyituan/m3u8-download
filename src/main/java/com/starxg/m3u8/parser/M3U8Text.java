package com.starxg.m3u8.parser;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * M3U8Text
 *
 * @author huangxingguang@lvmama.com
 * @date 2020-08-11 11:19
 */
@Getter
@Setter
@ToString
public class M3U8Text extends M3U8 {
    private List<M3U8Item> items = new ArrayList<>(0);

    public M3U8Text(URL url) {
        super(url);
    }

}
