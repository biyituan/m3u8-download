package com.starxg.m3u8.parser;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * M3U8ItemStatus
 * 
 * @author huangxingguang@lvmama.com
 * @date 2020-08-11 16:20
 */
@Getter
@AllArgsConstructor
public enum M3U8ItemStatus {
    WAITING("等待中"), RUNNING("下载中"), SUCCEEDED("下载成功"), FAILED("下载失败"), PAUSED("暂停中"), RETRYING("等待重试");

    private final String msg;

    @Override
    public String toString() {
        return getMsg();
    }
}
