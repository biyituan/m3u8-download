package com.starxg.m3u8.parser;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 多码率
 * 
 * @author huangxingguang@lvmama.com
 * @date 2020-08-11 11:21
 */
@Getter
@Setter
@ToString
public class M3U8Multi extends M3U8 {
    static final String PREFIX = "#EXT-X-STREAM-INF:";

    private List<Stream> streams = new ArrayList<>();

    public M3U8Multi(URL url) {
        super(url);
    }

    /**
     * 码率
     */
    @AllArgsConstructor
    @Getter
    public static class Stream {
        /**
         * 提示
         */
        private String text;
        /**
         * 地址
         */
        private URL url;

        @Override
        public String toString() {
            return getText();
        }
    }

}
