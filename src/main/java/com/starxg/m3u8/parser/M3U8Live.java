package com.starxg.m3u8.parser;

import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * 直播
 * 
 * @author huangxingguang@lvmama.com
 * @date 2020-08-19 16:20
 */
@Getter
@Setter
public class M3U8Live extends M3U8 {
    private Set<M3U8Item> items = new LinkedHashSet<>();

    M3U8Live(URL url) {
        super(url);
    }
}
