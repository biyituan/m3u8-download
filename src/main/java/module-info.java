module mtue.download {
    requires javafx.fxml;
    requires javafx.graphics;
    requires static lombok;
    requires javafx.controls;
    requires org.apache.commons.io;
    requires org.apache.commons.lang3;
    requires bcprov.jdk16;
    requires fastjson;
    requires java.sql;
    requires slf4j.api;
    requires okhttp3;
    requires okio;

    exports com.starxg.m3u8;
    exports com.starxg.m3u8.controller;
    exports com.starxg.m3u8.config;
    exports com.starxg.m3u8.parser;
    exports com.starxg.m3u8.gui;

    opens com.starxg.m3u8.controller to javafx.fxml;
    opens com.starxg.m3u8.config to fastjson;
    opens com.starxg.m3u8.parser to javafx.base;
}