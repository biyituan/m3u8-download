## 功能
- 多线程
- Proxy
- 支持Windows、MacOS

## 预览
![1](./1.png)


![2](./2.png)


![3](./3.png)


## 配置

- 基本
  - 同时下载 `多线程同时下载多个分片（建议根据网速配置，并不是越大越好）`
  - 自动合并 `下载完自动合并分片`
  - 删除分片 `合并完成后自动删除分片`
  - 重试次数 `下载失败重试的次数`
- Proxy
  - HTTP
  - SOCKS
- 请求头
  - 自定义请求头


## 下载地址(Windows & MacOS)
- [1.0.3 点我下载](https://lanzoui.com/b04ssvwaj)
    - 修复部分无法解密的问题
- [1.0.2](https://lanzoui.com/b04sp695i)
    - Proxy
    - 本地M3U8下载
    - 解密支持向量
    - 自定义并发数
    - 重试次数
    - 分片下载百分比
- [1.0.1](https://pan.lanzous.com/iInXIfqaded)
    - 删除部分无用链接
- ~~1.0.0~~

## 未来更新
- 直播流下载
- 批量下载

## 源码
- [Gitee](https://gitee.com/starxg/m3u8-download)

## 常见问题
1. 为什么有些 M3U8 无法下载？
    1. M3U8文件、Key、ts文件经过特殊加密
    2. 没有权限访问URL尝试配置请求头
2. MacOS 为什么无法打开
    1. 给权限：`sudo chmod -R 777 ./`
    2. 终端启动：`./start-mac.sh`  或 `bash ./start-mac.sh`